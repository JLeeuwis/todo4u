$(document).ready(function () {
    //Open & close item-detail
    $('.item-name, .item-collapsed').click(function () {
        let id = $(this).parent().data('id');
        if ($(".item-detail[data-id="+id+"]").is(":hidden"))
        {
            $(".item-detail[data-id="+id+"]").slideDown(350);
            $(".item-row[data-id="+id+"]").children(".item-collapsed").addClass("item-expand");
        } else {
            $(".item-detail[data-id="+id+"]").slideUp(350, function () {
                $(".item-detail[data-id="+id+"]").css("display", "");
            });
            $(".item-row[data-id="+id+"]").children(".item-collapsed").removeClass("item-expand");
        }
    });
});