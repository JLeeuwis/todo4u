//Dynamisch maken
$(document).ready(function(){
    //Show & hide create modal
    $('#modal-show').click(function () {
        $("#modal-create").removeClass('modal-hidden');
    });
    $('#modal-close').click(function () {
        $('#modal-create').addClass('modal-hidden');
    });

    //Edit list modal
    $('.list-edit').click(function () {
        let row = ($(this).parent().parent());
        $('#edit-list .modal-input[name="id"]').val($(row).data('id'));
        $('#edit-list .modal-input[name="name"]').val($(row).children(".list-name").children().text());
        $('#edit-list').removeClass('modal-hidden');
    });
    $('#edit-close').click(function () {
        $('#edit-list').addClass('modal-hidden');
    });

    //Edit item modal
    $('.item-edit').click(function () {
        let row = $(this).parent().parent();
        let detail = $('.item-detail[data-id='+$(row).data('id')+']');
        $('#edit-item .modal-input[name="id"]').val($(row).data('id'));
        $('#edit-item .modal-input[name="name"]').val($(row).children(".item-name").text());
        $('#edit-item .modal-input[name="description"]').val($(detail).children(".item-description").text());
        $('#edit-item .modal-input[name="startDate"]').val(formatDateTime($(detail).children("#item-start").children("p").text()));
        $('#edit-item .modal-input[name="endDate"]').val(formatDateTime($(detail).children("#item-end").children("p").text()));
        $('#edit-item').removeClass('modal-hidden');
    });
    $('#edit-close').click(function () {
        $('#edit-item').addClass('modal-hidden');
    });
});

function formatDateTime(dateTime)
{
    dateTime = dateTime.split(" ");
    let date = dateTime[0].split("-");
    let time = dateTime[1];
    return date[2]+"-"+date[1]+"-"+date[0]+"T"+time;
}