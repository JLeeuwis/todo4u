<?php

namespace Todo\Core;
use Todo\Controller;

class App
{
    protected $controller = 'home';

    protected $method = 'index';

    protected $params = [];

    protected $pages = [
        "home" => Controller\HomeController::class,
        "login" => Controller\LoginController::class,
        "list" => Controller\ItemListController::class,
        "item" => Controller\ItemController::class
    ];

    public function __construct()
    {
        $url = $this->parseURL();

        $url[0] = $url[0] ?? $this->controller;

        if(array_key_exists($url[0], $this->pages))
        {
            $this->controller = $url[0];
            unset($url[0]);
        }

        session_start();
        if (!(array_key_exists('user', $_SESSION)))
        {
            $_SESSION['user'] = null;
        }
        if (!(array_key_exists('message', $_SESSION)))
        {
            $_SESSION['message'] = null;
        }

        if($_SESSION['user'] == null)
        {
            $this->controller = 'login';
        }

        $this->controller = new $this->pages[$this->controller]();

        $url[1] = $url[1] ?? $this->method;

        if (method_exists($this->controller, $url[1]))
        {
            $this->method = $url[1];
            unset($url[1]);
        }

        $this->params = $url ? array_values($url) : [];

        call_user_func_array([$this->controller,$this->method],$this->params);
    }
    
    protected function parseURL(): ?array
    {
        $url = explode('/', rtrim($_SERVER['REQUEST_URI'], '/'));
        unset($url[0]);
        return array_values($url);
    }
}