<?php
namespace Todo\Core;
use PDO;

class Database
{
    protected static $instance = null;
    protected $conn;

    /**
     * Database constructor.
     */
    private function __construct()
    {
        $yamlParser = new YamlParser();
        $config = $yamlParser->getYamlFile();

        $host = $yamlParser->getConfig($config, 'host');
        $db = $yamlParser->getConfig($config, 'database');
        $user = $yamlParser->getConfig($config, 'user');
        $password = $yamlParser->getConfig($config, 'password');

        $dsn = 'mysql:host='.$host.';dbname='.$db;

        $options = [
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_EMULATE_PREPARES   => false
        ];

        try {
            $this->conn = new PDO($dsn, $user, $password, $options);
            return $this->conn;
        }
        catch(\PDOException $error){
            echo $error->getMessage();
            return false;
        }
    }

    /**
     * @return Database|null
     */
    public static function getInstance(): ?Database
    {
        if(!self::$instance) {
            self::$instance = new Database();
        }
        return self::$instance;
    }

    /**
     * @return PDO
     */
    public function getConnection(): PDO
    {
        return $this->conn;
    }
}