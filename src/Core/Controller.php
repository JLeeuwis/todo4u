<?php
namespace Todo\Core;

abstract class Controller
{
    protected $templateFile = "";
    protected $templateFolder = "";
    protected $param = [];

    /**
     * @param array $data
     * @return false|string
     */
    public function view($data = [])
    {
        ob_start();
        extract($data, EXTR_SKIP);
        include "./src/View//".$this->templateFolder."/".$this->templateFile;
        unset($_SESSION['message']);
        return ob_get_clean();
    }
}