<?php
namespace Todo\Core;

class YamlParser
{
    protected $config = '';
    /**
     * @return array|mixed
     */
    public function getYamlFile()
    {
        $defaultConfig = [
            'db_config' => [
                'host' => 'localhost',
                'database' => 'todo4u',
                'user' => 'root',
                'password' => ''
            ]
        ];

         return @yaml_parse_file('./config/config.yaml') ? yaml_parse_file('./config/config.yaml') : $defaultConfig;
    }

    /**
     * @param array $array
     * @param string $keyword
     * @return string|null
     */
    public function getConfig(array $array, string $keyword): ?string
    {
        foreach ($array as $key => $value)
        {
            if(is_array($value))
            {
                $this->getConfig($value, $keyword);
            }
            else {
                if ($key == $keyword)
                {
                    $this->config = $value;
                }
            }
        }
        return $this->config;
    }
}

