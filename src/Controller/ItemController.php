<?php
namespace Todo\Controller;
use \DateTime;
use Todo\Core\Controller;
use Todo\Domain\Model\Item;
use Todo\Domain\Repository\ItemRepository;

class ItemController extends Controller
{
    public function add()
    {
        $message = "Alle velden moeten ingevuld worden";
        $location = "/list/".$_SESSION['id'];
        if(array_key_exists('name', $_POST) && array_key_exists('description', $_POST) && array_key_exists('startDate', $_POST) && array_key_exists('endDate', $_POST))
        {
            if((strlen($name = $_POST['name']) > 0) && (strlen($description = $_POST['description']) > 0) && (strlen($startDate = $_POST['startDate']) > 0) && (strlen($endDate = $_POST['endDate']) > 0))
            {
                if (strlen($name) < 35) {

                    $startDate = new DateTime($startDate);
                    $endDate = new DateTime($endDate);

                    $data = [
                        'listId' => $_SESSION['id'],
                        'name' => $name,
                        'description' => $description,
                        'startDate' => $startDate->format("Y-m-d H:i:s"),
                        'endDate' => $endDate->format("Y-m-d H:i:s"),
                        'created' => date("Y-m-d H:i:s"),
                        'edited' => date("Y-m-d H:i:s")
                    ];

                    $itemRepository = new ItemRepository();
                    $itemRepository->create('item', $data);
                    $_SESSION['message'] = $name.' is aangemaakt';
                    header("location: ".$location);
                } else {
                    $_SESSION['message'] = 'De naam mag niet langer zijn dan 35 tekens';
                    header("location: ".$location);
                }

            } else {
                $_SESSION['message'] = $message;
                header("location: ".$location);
            }
        } else {
            $_SESSION['message'] = $message;
            header("location: ".$location);
        }

    }

    public function edit()
    {
        $message = "Dit item is niet beschikbaar";
        $location = "/list/".$_SESSION['id'];
        if(array_key_exists('id',$_POST) && array_key_exists('name', $_POST) && array_key_exists('description', $_POST) && array_key_exists('startDate', $_POST) && array_key_exists('endDate', $_POST))
        {
            if(is_numeric($id = $_POST['id']) && (strlen($name = htmlentities($_POST['name'])) > 0) && (strlen($description = htmlentities($_POST['description'])) > 0) && (strlen($startDate = htmlentities($_POST['startDate'])) > 0) && (strlen($endDate = htmlentities($_POST['endDate'])) > 0))
            {
                if (strlen($name) < 35) {
                    $itemRepository = new ItemRepository();
                    $item = $itemRepository->getById('Item',Item::class,$id);
                    if (array_key_exists(0, $item))
                    {
                        $item = $item[0];
                        if ($item->getListId() == $_SESSION['id'])
                        {
                            $data = [
                                'name' => $name,
                                'description' => $description,
                                'startDate' => $startDate,
                                'endDate' => $endDate,
                                'edited' => date("Y-m-d H:i:s")
                            ];

                            $itemRepository->update('item', $data, 'id = '.$id);
                            $_SESSION['message'] = 'Het item is gewijzigd';
                            header("location: ".$location);
                        } else {
                            $_SESSION['message'] = $message;
                            header("location: ".$location);
                        }
                    } else {
                        $_SESSION['message'] = $message;
                        header("location: ".$location);
                    }
                } else {
                    $_SESSION['message'] = 'De titel mag niet langer zijn dan 35 tekens';
                    header("location: ".$location);
                }
            } else {
                $_SESSION['message'] = $message;
                header("location: ".$location);
            }
        } else {
            $_SESSION['message'] = $message;
            header("location: ".$location);
        }
    }

    public function delete($id = "")
    {
        $message = "Dit item is niet beschikbaar";
        $location = "/list/".$_SESSION['id'];
        if(is_numeric($id))
        {
            $itemRepository = new ItemRepository();
            $item = $itemRepository->getById('item', Item::class, $id);
            if (array_key_exists(0, $item))
            {
                $item = $item[0];
                if ($item->getListId() == $_SESSION['id'])
                {
                    $itemRepository->delete('item', $id);
                    $_SESSION['message'] = 'Het item is verwijderd';
                    header("location: ".$location);
                } else {
                    $_SESSION['message'] = $message;
                    header("location: ".$location);
                }
            } else {
                $_SESSION['message'] = $message;
                header("location: ".$location);
            }
        } else {
            $_SESSION['message'] = $message;
            header("location: ".$location);
        }
    }

    public function complete($param = [])
    {
        $message = "Dit item is niet beschikbaar";
        $location = "/list/".$_SESSION['id'];
        if(is_numeric($id = $param ?? null))
        {
            $itemRepository = new ItemRepository();
            $item = $itemRepository->getById('Item', Item::class,$id);
            if (array_key_exists(0, $item))
            {
                $item = $item[0];
                if($item->getListId() == $_SESSION['id'])
                {
                    $completed = $item->getCompleted();
                    $completed = !$completed;
                    if($completed)
                    {
                        $message = "Dit item is voltooid";
                    } else {
                        $message = "Dit item is niet meer voltooid";
                    }

                    $data = [
                        'completed' => $completed
                    ];

                    $itemRepository->update('item', $data, 'id = '.$id);
                    $_SESSION['message'] = $message;
                    header("location: ".$location);
                }
                else {
                    $_SESSION['message'] = $message;
                    header("location: ".$location);
                }
            } else {
                $_SESSION['message'] = $message;
                header("location: ".$location);
            }
        } else {
            $_SESSION['message'] = $message;
            header("location: ".$location);
        }
    }

    public function filter()
    {
        if (array_key_exists('filterOption', $_POST)) {
            $_SESSION['filter'] = $_POST['filterOption'];
        }
        
        header('Location: /list/'.$_SESSION['id']);
    }
}