<?php

namespace Todo\Controller;
use Todo\Core\Controller;
use Todo\Domain\Repository\UserRepository;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->templateFolder = "Login";
    }

    public function index()
    {
        $this->templateFile = "Index.phtml";
        print $this->view();
    }

    public function validateUser()
    {
        if(array_key_exists('name',$_POST) && array_key_exists('password',$_POST))
        {
            if (strlen($name = htmlentities($_POST['name'])) > 0 && strlen($password = htmlentities($_POST['password'])) > 0)
            {
                $users = new UserRepository();
                if($user = $users->compareUsers(htmlentities($_POST['name']),htmlentities($_POST['password'])))
                {
                    $_SESSION['user'] = $user->getId();
                    header("Location: /");
                }
                else
                {
                    $_SESSION['message'] = 'Gebruikersnaam en/of wachtwoord zijn niet correct';
                    header("location: /login");
                }
            } else {
                $_SESSION['message'] = 'Beide velden moeten ingevuld worden';
                header("location: /login");
            }
        } else {
            $_SESSION['message'] = 'Beide velden moeten ingevuld worden';
            header("location: /login");
        }
    }

    public function logout()
    {
        unset($_SESSION['user']);
        header("Location: /login");
    }

    public function register()
    {
        $this->templateFile = "Register.phtml";
        print $this->view();
    }

    public function registerUser()
    {
        if (array_key_exists('name', $_POST) && array_key_exists('password', $_POST) && array_key_exists('repeatPassword', $_POST))
        {
            if (strlen($name = htmlentities($_POST['name'])) > 0 && strlen($password = htmlentities($_POST['password'])) > 0 && strlen($repeatPassword = htmlentities($_POST['repeatPassword'])) > 0)
            {
                if($password === $repeatPassword)
                {
                    $userRepository = new UserRepository();
                    $users = $userRepository->getUsers();
                    foreach ($users as $user)
                    {
                        if ($name == $user->getName())
                        {
                            $_SESSION['message'] = 'Deze naam is al ingebruik';
                            header("location: /login/register");
                            return;
                        }
                    }
                    $data = [
                        'name' => htmlentities($_POST['name']),
                        'password' =>  htmlentities($_POST['password'])
                    ];

                    $userRepository->create('user', $data);
                    $_SESSION['message'] = 'Uw account is gemaakt, u kunt nu inloggen';
                    header("location: /login/");
                }
                else {
                    $_SESSION['message'] = 'Wachtwoorden komen niet overeen';
                    header("location: /login/register");
                }
            } else {
                $_SESSION['message'] = 'Alle velden moeten worden ingevuld';
                header("location: /login/register");
            }
        } else {
            $_SESSION['message'] = 'Alle velden moeten worden ingevuld';
            header("location: /login/register");
        }
    }
}