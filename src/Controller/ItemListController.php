<?php
namespace Todo\Controller;
use Todo\Core\Controller;
use Todo\Domain\Model\ItemList;
use Todo\Domain\Model\User;
use Todo\Domain\Repository\ItemListRepository;
use Todo\Domain\Repository\ItemRepository;

class ItemListController extends Controller
{
    protected $items;
    protected $user;
    protected $author;

    public function __construct()
    {
        $this->templateFolder = "List";
    }

    public function index($id = "")
    {
        if ($id !== "")
        {
            if(is_numeric($id))
            {
                $listRepository = new ItemListRepository();
                $list = $listRepository->getById('itemlist',ItemList::class, $id);

                if(array_key_exists(0, $list)) {
                    $this->user = $_SESSION['user'] === $list[0]->getUserId() ? true : false;
                    $_SESSION['list'] = $list[0]->getName();
                    $_SESSION['id'] = $id;
                    $user = $listRepository->getById('user', User::class, $list[0]->getUserId());
                    if (array_key_exists(0,$user))
                    {
                        $this->author = $user[0]->getName();
                    }
                    else {
                        $this->author = "Unknown";
                    }
                }
                $filter = '';
                if (array_key_exists('filter', $_SESSION))
                {
                    $filter = $_SESSION['filter'];
                }
                $item = new ItemRepository();
                $this->items = $item->getItems($id, $filter);
            }
        }

        $this->templateFile = "Index.phtml";
        print $this->view();
    }

    public function add()
    {
        if(array_key_exists('name', $_POST))
        {
            if (strlen($name = htmlentities($_POST['name'])) < 1)
            {
                $_SESSION['message'] = 'Alle velden moeten ingevuld worden';
                header("Location: /");
            }
            else {
                if(strlen($name = htmlentities($_POST['name'])) > 35)
                {
                    $_SESSION['message'] = 'De naam mag niet langer zijn dan 35 tekens';
                    header("Location: /");
                } else {
                    $list = new ItemListRepository();
                    $data = [
                        'userId' => $_SESSION['user'],
                        'name' => $name,
                        'created' => date("Y-m-d H:i:s")
                    ];

                    $list->create('itemlist', $data);
                    $_SESSION['message'] = $name.' is aangemaakt';
                    header("location: /");
                }
            }
        } else {
            header("Location: /");
        }
    }

    public function edit()
    {
        $msg = 'Deze lijst is niet beschikbaar';
        if (array_key_exists('id', $_POST) && array_key_exists('name', $_POST))
        {
            if (is_numeric(htmlentities($id = $_POST['id'])) && (strlen(htmlentities($name = $_POST['name']))) > 0) {
                if(strlen($name) < 35) {
                    $listRepository = new ItemListRepository();
                    $list = $listRepository->getById('itemlist',ItemList::class, $id);
                    if (array_key_exists(0, $list))
                    {
                        $list = $list[0];
                        if ($list->getUserId() === $_SESSION['user'])
                        {
                            $data = [
                                'name' => $name
                            ];

                            $listRepository->update('itemlist', $data, 'id = '.$id);
                            $_SESSION['message'] = 'De lijst is gewijzigd';
                            header("location: /");
                        } else {
                            $_SESSION['message'] = $msg;
                            header("location: /");
                        }
                    } else {
                        $_SESSION['message'] = $msg;
                        header("location: /");
                    }
                } else {
                    $_SESSION['message'] = 'De naam mag niet langer zijn dan 35 tekens';
                    header("Location: /");
                }
            } else {
                $_SESSION['message'] = $msg;
                header('Location: /');
            }
        }
        else {
            $_SESSION['message'] = $msg;
            header('Location: /');
        }
    }

    public function delete($id = "")
    {
        $msg = 'De lijst kon niet worden verwijderd';
        if ($id != "")
        {
            if (is_numeric($id))
            {
                $listRepository = new ItemRepository();
                if (array_key_exists(0, $list = $listRepository->getById('itemlist',ItemList::class,$id)))
                {
                    $list = $list[0];
                    if ($list->getUserId() === $_SESSION['user'])
                    {
                        $listRepository->delete('itemlist', $id);
                        $_SESSION['message'] = 'De lijst is verwijderd';
                        header("location: /");
                    } else {
                        $_SESSION['message'] = 'Deze lijst is niet beschikbaar';
                        header('Location: /');
                    }
                } else {
                    $_SESSION['message'] = $msg;
                    header('Location: /');
                }

            } else {
                $_SESSION['message'] = $msg;
                header('Location: /');
            }
        } else {
            $_SESSION['message'] = $msg;
            header('Location: /');
        }

    }
}