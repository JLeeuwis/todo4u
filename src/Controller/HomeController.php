<?php

namespace Todo\Controller;
use Todo\Core\Controller;
use Todo\Domain\Repository\ItemListRepository;

class HomeController extends Controller
{
    protected $lists = [];

    public function __construct()
    {
        $this->templateFolder = "Home";
    }

    public function index()
    {
        $itemList = new ItemListRepository();
        $this->lists = $itemList->getLists($_SESSION['user']);
        
        $this->templateFile = "Index.phtml";
        print $this->view();
    }
}