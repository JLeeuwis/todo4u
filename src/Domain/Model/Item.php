<?php

namespace Todo\Domain\Model;
use \DateTime;

class Item extends AbstractModel
{
    /**
     * @var int
     */
    protected $listId;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $startDate;

    /**
     * @var string
     */
    protected $endDate;

    /**
     * @var int
     */
    protected $completed;

    /**
     * @var string
     */
    protected $created;

    /**
     * @var string
     */
    protected $edited;

    /**
     * @return int
     */
    public function getListId(): int
    {
        return $this->listId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getStartDate(): string
    {
        $this->startDate = new DateTime($this->startDate);
        return $this->startDate->format('d-m-Y H:i');
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getEndDate(): string
    {
        if(!($this->endDate instanceof DateTime))
        {
            $this->endDate = new DateTime($this->endDate);
            $this->endDate = $this->endDate->format('d-m-Y H:i');

        }
        return $this->endDate;
    }

    /**
     * @return int
     */
    public function getCompleted(): int
    {
        return $this->completed;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getCreated(): string
    {
        $this->created = new DateTime($this->created);
        return $this->created->format('d-m-Y H:i');
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getEdited(): string
    {
        $this->edited = new DateTime($this->edited);
        return $this->edited->format('d-m-Y H:i');
    }

    /**
     * @param int $listId
     */
    public function setListId(int $listId): void
    {
        $this->listId = $listId;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @param string $startDate
     */
    public function setStartDate(string $startDate): void
    {
        $this->startDate = $startDate;
    }

    /**
     * @param string $endDate
     */
    public function setEndDate(string $endDate): void
    {
        $this->endDate = $endDate;
    }

    /**
     * @param int $completed
     */
    public function setCompleted(int $completed): void
    {
        $this->completed = $completed;
    }

    /**
     * @param string $created
     */
    public function setCreated(string $created): void
    {
        $this->created = $created;
    }

    /**
     * @param string $edited
     */
    public function setEdited(string $edited): void
    {
        $this->edited = $edited;
    }
}