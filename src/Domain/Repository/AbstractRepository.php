<?php
namespace Todo\Domain\Repository;
use Todo\Core\Database;

abstract class AbstractRepository
{
    protected $conn;

    public function __construct()
    {
        $this->conn = Database::getInstance()->getConnection();
    }

    /**
     * @param $table
     * @param $class
     * @param $id
     * @return array
     */
    public function getById($table, $class, $id): array
    {
        return $this->fetchClassWhere($table,$class, 'id = '.$id);
    }

    /**
     * @param $table
     * @param $class
     * @return array
     */
    public function fetchClass($table, $class): array
    {
        $sql = 'SELECT * FROM '.$table;
        $statement = $this->conn->prepare($sql);
        $statement->setFetchMode(\PDO::FETCH_CLASS, $class);
        $statement->execute();
        $row = $statement->fetchAll();
        $statement = null;

        return $row;
    }

    /**
     * @param string $table
     * @param string $class
     * @param string $where
     * @return array
     */
    public function  fetchClassWhere(string $table, string $class, string $where): array
    {
        $sql = 'SELECT * FROM '.$table.' WHERE '.$where;
        $statement = $this->conn->prepare($sql);
        $statement->setFetchMode(\PDO::FETCH_CLASS, $class);
        $statement->execute();
        $row = $statement->fetchAll();
        $statement = null;

        return $row;
    }

    /**
     * @param string $table
     * @param array $data
     */
    public function create(string $table, array $data)
    {
        $columns = '';
        $values = '';
        $count = count($data);

        foreach ($data as $key => $value)
        {
            if (--$count <= 0)
            {
                $columns .= $key;
                $values .= "?";
            } else {
                $columns .= $key.',';
                $values .= "?, ";
            }
        }

        $sql = "INSERT INTO ".$table." (".$columns.") VALUES(".$values.")";
        $statement = $this->conn->prepare($sql);
        $statement->execute(array_values($data));
        $statement = null;
    }

    /**
     * @param string $table
     * @param array $data
     * @param string $where
     */
    public function update(string $table, array $data, string $where)
    {
        $values = '';
        $count = count($data);

        foreach ($data as $key => $value)
        {
            if (--$count <= 0)
            {
                $values .= $key." = ?";
            } else {
                $values .= $key." = ?, ";
            }
        }
        $sql = "UPDATE ".$table." SET ".$values." WHERE ".$where;
        $statement = $this->conn->prepare($sql);
        $statement->execute(array_values($data));
        $statement = null;
    }

    /**
     * @param string $table
     * @param int $id
     */
    public function delete(string $table ,int $id)
    {
        $sql = "DELETE FROM ".$table." WHERE id = ?";
        $statement = $this->conn->prepare($sql);
        $statement->execute([$id]);
        $statement = null;
    }
}