<?php
namespace Todo\Domain\Repository;
use Todo\Domain\Model\User;

class UserRepository extends AbstractRepository
{
    /**
     * @return array
     */
    public function getUsers()
    {
        return $this->fetchClass('user',User::class);
    }

    /**
     * @param $name
     * @param $password
     * @return array|mixed
     */
    public function compareUsers($name, $password)
    {
        $users = $this->getUsers();
        $registerdUser = [];

        foreach ($users as $user)
        {
            if($name === $user->getName() && $password === $user->getPassword())
            {
                $registerdUser = $user;
            }
        }

        return $registerdUser;
    }
}