<?php
namespace Todo\Domain\Repository;

use Todo\Domain\Model\Item;

class ItemRepository extends AbstractRepository
{
    public function getItems($id, $filter = '')
    {
        if ($filter != '')
        {
            $filter = explode('-',$filter);
            $filter = $filter[0].' '.$filter[1];
        } else {
            $filter = '`completed` ASC, `endDate` ASC';
        }
        return $this->fetchClassWhere('item',Item::class,'listId = '.$id.' ORDER BY '.$filter);
    }
}