<?php
namespace Todo\Domain\Repository;
use Todo\Domain\Model\ItemList;

class ItemListRepository extends AbstractRepository
{
    public function getLists($id): array
    {
        return $this->fetchClassWhere('itemlist',ItemList::class, 'userId = '.$id);
    }
}